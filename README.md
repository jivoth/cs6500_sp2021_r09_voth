# Jonathan Voth
## Master of Science in Analytics

**List the 3 Hive Queries you wrote.**

Query #1: SELECT Zipcode, Income FROM income WHERE Income < 50000;

Query #2: SELECT ID, Zipcode, Income FROM income WHERE Income > 44382 ORDER By Income; (I did a previous statement that found the average income, then hard coded it into Query #2.)

Query #3: SELECT Zipcode, Income FROM income WHERE (Income > 30000 AND Income < 60000);

**What technical errors did you experience? Please list them explicitly and identify how you corrected them.**

The first technical error I ran into was finding the correct place to mount my Workspace so I could access the data. Next, I had a few issues uploading the data from HDFS to Hive. There were problems with the format of the table. At first, I noticed a problem with how I initially formatted the table. I forgot to skip over the header line. I fixed that but still had issues. I reached out to a few classmates who helped my with my ROW FORMAT line of code. Then, all was good with my table.

As always, I ran into simple syntax errors, but I obviously worked through those.

**What conceptual difficulties did you experience?**

I didn't really have any conceptual difficulties this week. From the Discord chat, some fellow classmates were discussing the ability to run Query #2 all in one statement using a subquery. A subquery is simply a query within a query, however, I couldn't figure out the syntax for the particular query we needed to run. I read that Hive supports subqueries in the FROM and WHERE clauses and only one subquery can be run per statement. This is valuable information moving forward. I felt I had a solid work-around for the soluntion. Another issue I ran into was my output was sorted weird. I used an ORDER BY clause to order by income, but the output started with the incomes greater than $100,000 and increased, then jumped back down into the $40,000 range. I don't know why this occurred.

**How much time did you spend on each part of the assignment?**

Overall, I spent about between 6-8 hours on this assignment. The reading and video took about 45 minutes each. Once I was able to upload the data in the correct format, everything went smoothly. The queries took only a couple seconds to run. I had to store the output of each query in a file in HDFS and then download that file to my local computer. This added a little extra time but not much. Thinking of a third query to run honestly took more time than the previous two queries combined. I ended up running a query that got the zip code and income of what I classified as the "middle class". There were a total of 69,438 zip codes whose median income was between $30,000 and $60,000. This represents just under 70% of the whole data set, which I thought was interesting. I found those counts by doing a "SELECT COUNT(*)" statement.

Using Docker and Gitlab added minimal time to the assignment.

**What was the hardest part of this assignment?**

One of the hardest parts of this assignment was correctly uploading the data into Hive in the correct format. It was slightly different from the tutorial video. I had to work through it with a few classmates to get the proper format and path to the data set. Also, I had somewhat of a hard time thinking of my own query to run. I wanted it to be somewhat different from the first two but also have meaning. I think I found a good balance. Moreover, Query #2 gave me some issues with the subquery syntax/structure. I kind of took the easy way out with that part because I just hard coded in the average income. Solution is still the same though.

**What was the easiest part of this assignment?**

The easiest part of the assignment was writing the first query. HiveQL is very similar to SQL which I am familiar with and have with with in the past. It is readable and intuitive to write. I also found it quite easy to navigate Hive in the terminal. It was easy to run the queries and didn't take much time. The second query gave me a little trouble, but I found a solution that works. Before this assignment, I forgot how much I enjoy SQL and now Hive QL because of its simplicity.

**What advice would you give someone doing this assignment in the future?**

My advice to someone would be to rely on past knowledge and courses. HiveQL is similar to SQL which I learned last semester but haven't used in a while. It took a bit to get the basics down again, but it eventually came back to me as I was working on the reflection. Also, use your classmates as a resource. Work through your issues with them and you'll both be better for it. I have found that collaboration is very helpful.

**What did you actually learn from doing this assignment?**

I learned about Hive. Hive integrates with HDFS and runs MapReduce jobs using the query language HiveQl. It is similar to SQL. Hive contains improvements for optimization and storage compared to the original MapReduce. It is more simple and user friendly. We were exposed to simple jobs like counts and more complex queries that use subqueries to run. I also learned how to save the output in HDFS using a OVERWRITE DIRECTORY command. Hive keeps it simple and is much preferred over MapReduce.

**Why does what I learned matter both academically and practically?**

I am just beginning the process of looking for jobs after gradution. I recently came across a job posting looking for experience in Spark and Hive. That is why this matters. I don't have any work experience in this area, however, this academic work is something I can put on my resume and use as talking points with potential employers. The use of data is growing so much that these skills are and will continue to be very attractive in the professional world.